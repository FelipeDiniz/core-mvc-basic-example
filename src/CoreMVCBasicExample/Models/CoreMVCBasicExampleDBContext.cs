﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace CoreMVCBasicExample.Models
{
    public partial class CoreMVCBasicExampleDBContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            #warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
            optionsBuilder.UseSqlServer(@"Server=LAPTOP-MB5J3N6O\SQLEXPRESS;Database=CoreMVCBasicExampleDB;Trusted_Connection=True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Pages>(entity =>
            {
                entity.Property(e => e.PageName)
                    .IsRequired()
                    .HasColumnType("varchar(127)");
            });

            modelBuilder.Entity<PagesPermissions>(entity =>
            {
                entity.HasOne(d => d.Page)
                    .WithMany(p => p.PagesPermissions)
                    .HasForeignKey(d => d.PageId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_PagesPermissions_Pages");

                entity.HasOne(d => d.Permission)
                    .WithMany(p => p.PagesPermissions)
                    .HasForeignKey(d => d.PermissionId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_PagesPermissions_Permissions");
            });

            modelBuilder.Entity<Permissions>(entity =>
            {
                entity.Property(e => e.PermissionName)
                    .IsRequired()
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<Users>(entity =>
            {
                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasColumnType("varchar(50)");

                entity.HasOne(d => d.Permission)
                    .WithMany(p => p.Users)
                    .HasForeignKey(d => d.PermissionId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Users_Permissions");
            });
        }

        public virtual DbSet<Pages> Pages { get; set; }
        public virtual DbSet<PagesPermissions> PagesPermissions { get; set; }
        public virtual DbSet<Permissions> Permissions { get; set; }
        public virtual DbSet<Users> Users { get; set; }
    }
}