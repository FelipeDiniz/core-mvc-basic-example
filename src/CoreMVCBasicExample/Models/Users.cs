﻿using System;
using System.Collections.Generic;

namespace CoreMVCBasicExample.Models
{
    public partial class Users
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
        public int PermissionId { get; set; }

        public virtual Permissions Permission { get; set; }
    }
}
