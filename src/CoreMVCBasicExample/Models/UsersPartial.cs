﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

//This partial class is used to bind new methods and give some attributes to another partial class with the same name. In this case, once we are making a DB First application, if we make any changes at the DB, when we make the reverse engineering the visual studio will delet and recreate all the classes autogenereted from database but our partial classes will be save from any change.
namespace CoreMVCBasicExample.Models
{
    [ModelMetadataType(typeof(UsersMetadata))]
    public partial class Users
    {
    }

    public class UsersMetadata
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Last name")]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        public int PermissionId { get; set; }
    }


}
