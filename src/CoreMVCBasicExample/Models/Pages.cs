﻿using System;
using System.Collections.Generic;

namespace CoreMVCBasicExample.Models
{
    public partial class Pages
    {
        public Pages()
        {
            PagesPermissions = new HashSet<PagesPermissions>();
        }

        public int Id { get; set; }
        public string PageName { get; set; }

        public virtual ICollection<PagesPermissions> PagesPermissions { get; set; }
    }
}
