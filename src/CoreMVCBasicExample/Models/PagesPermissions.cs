﻿using System;
using System.Collections.Generic;

namespace CoreMVCBasicExample.Models
{
    public partial class PagesPermissions
    {
        public int Id { get; set; }
        public int PageId { get; set; }
        public int PermissionId { get; set; }

        public virtual Pages Page { get; set; }
        public virtual Permissions Permission { get; set; }
    }
}
