﻿using System;
using System.Collections.Generic;

namespace CoreMVCBasicExample.Models
{
    public partial class Permissions
    {
        public Permissions()
        {
            PagesPermissions = new HashSet<PagesPermissions>();
            Users = new HashSet<Users>();
        }

        public int Id { get; set; }
        public string PermissionName { get; set; }

        public virtual ICollection<PagesPermissions> PagesPermissions { get; set; }
        public virtual ICollection<Users> Users { get; set; }
    }
}
