﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace CoreMVCBasicExample.Useful
{
    public class MyConvert
    {
        public string ToMD5(string input)
        {
            try
            {
                StringBuilder sBuilder = new StringBuilder();
                using (MD5 md5Hash = MD5.Create())
                {
                    // Convert the input string to a byte array and compute the hash.
                    byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

                    // Create a new Stringbuilder to collect the bytes
                    // and create a string.

                    // Loop through each byte of the hashed data 
                    // and format each one as a hexadecimal string.
                    for (int i = 0; i < data.Length; i++)
                    {
                        sBuilder.Append(data[i].ToString("x2"));
                    }
                    // Return the hexadecimal string.
                    return sBuilder.ToString();
                }
            }
            catch (ArgumentNullException)
            {
                return string.Empty;

            }

        }

        // Verify a hash against a string.
        public bool VerifyMd5(string input, string hash)
        {
            // Hash the input.
            string hashOfInput = ToMD5(input);

            // Create a StringComparer an compare the hashes.
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;

            if (0 == comparer.Compare(hashOfInput, hash))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}

