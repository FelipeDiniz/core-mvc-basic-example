﻿using CoreMVCBasicExample.Handlers;
using CoreMVCBasicExample.Models;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CoreMVCBasicExample
{
    public class PageHandler : AuthorizationHandler<PageRequirement, Pages>
    {
        protected override Task HandleRequirementAsync(
            AuthorizationHandlerContext context,
            PageRequirement requirement,
            Pages resource)
        {
            foreach(PagesPermissions permission in resource.PagesPermissions)
                if (permission.Permission.PermissionName == context.User.FindFirst(ClaimTypes.Role).Value)
                    context.Succeed(requirement);

            return Task.FromResult(0);
        }
    }
}
