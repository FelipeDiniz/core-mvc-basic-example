﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreMVCBasicExample.Models;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using Microsoft.AspNetCore.Http.Authentication;
using Microsoft.AspNetCore.Authorization;
using CoreMVCBasicExample.Useful;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace CoreMVCBasicExample.Controllers
{
    [AllowAnonymous]
    public class AccountController : AuthController
    {
        public AccountController(CoreMVCBasicExampleDBContext context) : base(context)
        {
        }

        [HttpGet]
        public IActionResult Login(string returnUrl = null) //(string returnUrl = null) defines a default value to an optional parameter. you do not need to pass "returnUrl" to the method. if you do that, the system will create this variable as null
        {
            ViewBag.returnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        public IActionResult Login(string userName, string password, string returnUrl = null)
        {
            MyConvert convert = new MyConvert();
            string newPassword = convert.ToMD5(password);
            try
            {
                Users user = db.Users.Where(x => x.UserName == userName && x.Password == newPassword).Single(); //check lambda expressions if you do not understood https://msdn.microsoft.com/en-us/library/bb397687.aspx
                Authentication(user);
                var a = User.Identity.ToString();
                return RedirectToLocal(returnUrl);
            }
            catch (InvalidOperationException e)
            {
                ViewBag.ErrorMessage = "Check your user name and password or ask and admin to register you";
                return View();
            }
            catch(Exception e)
            {
                ViewBag.ErrorMessage = "Please, try again later or send the following code to our support:" + e.HResult;
                return View();
            }
        }

        private IActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public IActionResult Forbidden()
        {
            return View();
        }
    }
}
