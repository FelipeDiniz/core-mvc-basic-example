﻿using System;
using System.Linq;
using CoreMVCBasicExample.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CoreMVCBasicExample.Useful;
using Microsoft.AspNetCore.Authorization;
using System.Threading.Tasks;
using CoreMVCBasicExample.Handlers;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860
namespace CoreMVCBasicExample.Controllers
{
    [Authorize(Roles = "Admin")]
    public class UsersController : Controller
    {
        public CoreMVCBasicExampleDBContext db;
        IAuthorizationService authService;
        public UsersController(CoreMVCBasicExampleDBContext context, IAuthorizationService authorizationService)
        {
            db = context;
            authService = authorizationService;
        }

        // GET: Users
        public IActionResult Index()
        {
            var users = db.Users.Include(u => u.Permission);
            return View(users.ToList());
        }

        // GET: Users/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Error", "Home");
            }
            Users users = db.Users.Where(x => x.Id == id).SingleOrDefault();
            if (users == null)
            {
                return RedirectToAction("Error", "Home");
            }
            return View(users);
        }

        // GET: Users/Create
        public async Task<IActionResult> Create()
        {
            Pages page = db.Pages.Where(x => x.PageName == "Users - Create" ).SingleOrDefault();
            page.PagesPermissions = db.PagesPermissions.Where(x => x.PageId == page.Id).ToList();
            for (int i = 0; i < page.PagesPermissions.Count; i++)
                page.PagesPermissions.ElementAt(i).Permission = db.Permissions.Where(x => x.Id == page.PagesPermissions.ElementAt(i).PermissionId).SingleOrDefault();

            if (page == null)
            {
                return new NotFoundResult();
            }

            if (await authService.AuthorizeAsync(User, page, new PageRequirement()))
            {
                ViewBag.PermissionId = new SelectList(db.Permissions, "Id", "PermissionName");
                return View();
            }
            else
            {
                return new ChallengeResult();
            }

        }

        // POST: Users/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(string UserName, string Name, string LastName, string Password, int PermissionId)
        {
            MyConvert convert = new MyConvert();
            Users user = new Users
            {
                UserName = UserName,
                Name = Name,
                LastName = LastName,
                Password = convert.ToMD5(Password),
                PermissionId = PermissionId
            };
            if (ModelState.IsValid)
            {
                db.Users.Add(user);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.PermissionId = new SelectList(db.Permissions, "Id", "PermissionName", user.PermissionId);
            return View(user);
        }

        // GET: Users/Edit/5
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Error", "Home");
            }
            Users users = db.Users.Where(x => x.Id == id).SingleOrDefault();
            if (users == null)
            {
                return RedirectToAction("Error", "Home");
            }
            ViewBag.PermissionId = new SelectList(db.Permissions, "Id", "PermissionName", users.PermissionId);
            return View(users);
        }

        // POST: Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int Id, string UserName, string Name, string LastName, int PermissionId)
        {
            Users user = new Users
            {
                Id = Id,
                UserName = UserName,
                Name = Name,
                LastName = LastName,
                PermissionId = PermissionId
            };

            if (ModelState.IsValid)

            {
                
                var entry = db.Entry(user);
                entry.State = EntityState.Modified;
                entry.Property("Password").IsModified = false; //you can use this to not update a specific property

                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PermissionId = new SelectList(db.Permissions, "Id", "PermissionName", user.PermissionId);
            return View(user);
        }

        // GET: Users/Delete/5
        public IActionResult Delete(int? id)
        {
            try
            {
                if (id == null)
                {
                    return RedirectToAction("Error", "Home");
                }
                Users users = db.Users.Where(x => x.Id == id).SingleOrDefault();
                if (users == null)
                {
                    return RedirectToAction("Error", "Home");
                }
                return View(users);
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = "Please, try again later or send the following code to our support:" + e.HResult;
                return View("Index");
            }
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            Users users = db.Users.Where(x => x.Id == id).Single();
            db.Users.Remove(users);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
