﻿using CoreMVCBasicExample.Models;
using Microsoft.AspNetCore.Http.Authentication;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CoreMVCBasicExample.Controllers
{
    //you can use this controller to create your own authentication methods. I ussualy use it to logout too
    public class AuthController : Controller
    {
        public CoreMVCBasicExampleDBContext db;

        public AuthController(CoreMVCBasicExampleDBContext context)
        {
            db = context;
        }
        public async void Authentication(Users user)
        {
            var claims = new List<Claim>();
            claims.Add(new Claim(ClaimTypes.Name, user.Name, ClaimValueTypes.String));
            claims.Add(new Claim(ClaimTypes.Role, db.Permissions.Where(x => x.Id == user.PermissionId).Single().PermissionName, ClaimValueTypes.String));

            var userIdentity = new ClaimsIdentity("SecureLogin");
            userIdentity.AddClaims(claims);
            var userPrincipal = new ClaimsPrincipal(userIdentity);

            await HttpContext.Authentication.SignInAsync("Cookie", userPrincipal,
                        new AuthenticationProperties
                        {
                            ExpiresUtc = DateTime.UtcNow.AddMinutes(20),
                            IsPersistent = false,
                            AllowRefresh = false
                        });
        }

        public IActionResult Logout()
        {
            //Since we can't remove cookies we just expires it
            HttpContext.Authentication.SignOutAsync("Cookie", new AuthenticationProperties
            {
                ExpiresUtc = DateTime.UtcNow.AddDays(-1d),
                IsPersistent = false,
                AllowRefresh = false
            });
            return RedirectToAction("Index", "Home");
        }
    }
}
